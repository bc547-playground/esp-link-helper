
# UPDATE 20160402 
**This tool is not any longer needed! Try using avrflash that's distributed with the esp-link firmware itself**



# Information:

**For more information, take a look at https://github.com/jeelabs/esp-link/issues/63**

The code is in perl and is developed/tested on linux. It should also run on windows using e.g. [ActiveState perl](http://www.activestate.com/activeperl).

# Usage:

* Edit the file and change $esplink to the ip you use for your esp-link module.

* Open a terminal and run 'perl esp-link-helper.pl'. You should see

```
SERVER Waiting for client connection on port 5000
```

* For avrdude, use the ip of the host the proxy is running on, port 5000. (e.g. -P net:192.168.1.251:5000)

* After avrdude is done, you can find a full trace of all communication in the file *debugtrace.txt* 

# Avrdude example:

The following snippet can be used to test the proxy with a atmega328p mcu if you have no hex file at hand. It just reads the mcu fuses and prints their values on stdout. 

```
/usr/bin/avrdude -V -p atmega328p -D -c arduino -b 57600 -P net:127.0.0.1:5000 -U lfuse:r:-:i
```

**Notes:**
* The baud rate (-b 57600) is ignored when using a network port (e.g. net:127.0.0.1:5000). You must set the correct programming baudrate in esp-link itself.
* You need to change the network port and configure it for your network. 127.0.0.1 is only valid if you run the proxy on the same host as avrdude.